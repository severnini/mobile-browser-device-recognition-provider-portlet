package br.com.severnini.portal.mobile.device;

/**
 *
 * Keys and constants
 *
 * @author Luiz Fernando Severnini
 *
 */
public class MobileKeys {

	public static final String DEVICE_MOBILE_BROWSER = "Mobile browser";

	public static final String PROP_PREFIX = "mobilebrowser.regex.";
	public static final String PROP_TABLETS = "tablets";
	public static final String PROP_PATTERN_TABLETS = "pattern.tablets";
	public static final String PROP_PATTERN_STARTS_WITH = "pattern.starts.with";
	public static final String PROP_PATTERN_CONTAINS = "pattern.contains";

	public static final String DEVICE = "DEVICE";
	public static final String USER_AGENT = "User-Agent";

}
