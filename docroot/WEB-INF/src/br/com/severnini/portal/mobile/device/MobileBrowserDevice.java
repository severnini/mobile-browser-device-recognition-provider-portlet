package br.com.severnini.portal.mobile.device;


import com.liferay.portal.kernel.mobile.device.AbstractDevice;
import com.liferay.portal.kernel.mobile.device.Capability;
import com.liferay.portal.kernel.mobile.device.Dimensions;
import com.liferay.portal.kernel.util.StringPool;

import java.util.Collections;
import java.util.Map;

/**
 *
 * Mobile browser device
 *
 * @author Luiz Fernando Severnini
 *
 */
public class MobileBrowserDevice extends AbstractDevice {

	private static final long serialVersionUID = -6698031903597917015L;

	public MobileBrowserDevice() {
		super();
	}

	public MobileBrowserDevice(String osVersion, boolean tablet) {
		this();
		this.osVersion = osVersion;
		this.tablet = tablet;
	}

	@Override
	public String getBrand() {
		return osVersion;
	}

	@Override
	public String getBrowser() {
		return MobileKeys.DEVICE_MOBILE_BROWSER;
	}

	@Override
	public String getBrowserVersion() {
		return StringPool.BLANK;
	}

	@Override
	public Map<String, Capability> getCapabilities() {
		return Collections.emptyMap();
	}

	@Override
	public String getCapability(String name) {
		return StringPool.BLANK;
	}

	@Override
	public String getModel() {
		return MobileKeys.DEVICE_MOBILE_BROWSER;
	}

	@Override
	public String getOS() {
		return MobileKeys.DEVICE_MOBILE_BROWSER;
	}

	@Override
	public String getOSVersion() {
		return osVersion;
	}

	@Override
	public String getPointingMethod() {
		return StringPool.BLANK;
	}

	@Override
	public Dimensions getScreenSize() {
		return Dimensions.UNKNOWN;
	}

	@Override
	public boolean hasQwertyKeyboard() {
		return false;
	}

	@Override
	public boolean isTablet() {
		return tablet;
	}

	@Override
	public Dimensions getScreenPhysicalSize() {
		return Dimensions.UNKNOWN;
	}

	@Override
	public Dimensions getScreenResolution() {
		return Dimensions.UNKNOWN;
	}

	private String osVersion;
	private boolean tablet;
}
