package br.com.severnini.portal.mobile.device;


import com.liferay.portal.kernel.mobile.device.Capability;
import com.liferay.portal.kernel.mobile.device.KnownDevices;
import com.liferay.portal.kernel.mobile.device.VersionableName;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * Mobile browser known devices
 *
 * @author Luiz Fernando Severnini
 *
 */
public class MobileBrowserKnownDevices implements KnownDevices {

	@Override
	public Set<VersionableName> getBrands() {
		return BRANDS_VN;
	}

	@Override
	public Set<VersionableName> getBrowsers() {
		return BROWSERS_VN;
	}

	@Override
	public Map<Capability, Set<String>> getDeviceIds() {
		return Collections.emptyMap();
	}

	@Override
	public Set<VersionableName> getOperatingSystems() {
		return OS_VN;
	}

	@Override
	public Set<String> getPointingMethods() {
		return Collections.emptySet();
	}

	@Override
	public void reload() throws Exception {
	}

	private static final Set<VersionableName> BRANDS_VN;
	private static final Set<VersionableName> BROWSERS_VN;
	private static final Set<VersionableName> OS_VN;

	static {
		VersionableName mobileBrowser = new VersionableName(MobileKeys.DEVICE_MOBILE_BROWSER);

		BRANDS_VN = new HashSet<VersionableName>(1);
		BROWSERS_VN = new HashSet<VersionableName>(1);
		OS_VN = new HashSet<VersionableName>(1);

		BRANDS_VN.add(mobileBrowser);
		BROWSERS_VN.add(mobileBrowser);
		OS_VN.add(mobileBrowser);
	}

}
