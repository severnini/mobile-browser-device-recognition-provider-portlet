package br.com.severnini.portal.mobile.device;

import static br.com.severnini.portal.mobile.device.MobileKeys.PROP_PATTERN_CONTAINS;
import static br.com.severnini.portal.mobile.device.MobileKeys.PROP_PATTERN_STARTS_WITH;
import static br.com.severnini.portal.mobile.device.MobileKeys.PROP_PATTERN_TABLETS;
import static br.com.severnini.portal.mobile.device.MobileKeys.PROP_PREFIX;
import static br.com.severnini.portal.mobile.device.MobileKeys.PROP_TABLETS;
import static br.com.severnini.portal.mobile.device.MobileKeys.USER_AGENT;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mobile.device.Device;
import com.liferay.portal.kernel.mobile.device.DeviceCapabilityFilter;
import com.liferay.portal.kernel.mobile.device.DeviceRecognitionProvider;
import com.liferay.portal.kernel.mobile.device.KnownDevices;
import com.liferay.portal.kernel.mobile.device.UnknownDevice;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.portlet.PortletProps;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * Mobile browser device recognition provider
 *
 * @author Luiz Fernando Severnini
 *
 */
public class MobileBrowserDeviceRecognitionProvider implements DeviceRecognitionProvider {

	@Override
	public Device detectDevice(HttpServletRequest request) {
		Device device = null;

		if (_log.isDebugEnabled()) {
			_log.debug("Device detection: USER-AGENT=" + request.getHeader(USER_AGENT));
		}

		String uaRequest = request.getHeader(USER_AGENT);

		if (Validator.isNotNull(uaRequest)) {
			uaRequest = uaRequest.toLowerCase();

			boolean isTablet = uaRequest.matches(REGEX_PATTERN_TABLETS);

			if (uaRequest.matches(REGEX_PATTERN_CONTAINS) || uaRequest.substring(0, 4).matches(REGEX_PATTERN_STARTS_WITH)) {
				device = new MobileBrowserDevice(request.getHeader(USER_AGENT), isTablet);
			}
		}

		if (device == null) {
			device = UnknownDevice.getInstance();
		}

		if (_log.isDebugEnabled()) {
			_log.debug("Device: " + device);
		};

		return device;
	}

	@Override
	public KnownDevices getKnownDevices() {
		return KNOWN_DEVICES;
	}

	@Override
	public void reload() throws Exception {
	}

	@Override
	public void setDeviceCapabilityFilter(
			DeviceCapabilityFilter deviceCapabilityFilter) {
		if (_log.isDebugEnabled()) {
			_log.debug("setDeviceCapabilityFilter()");
		}
	}

	private static Log _log = LogFactoryUtil.getLog(MobileBrowserDeviceRecognitionProvider.class);

	private static final KnownDevices KNOWN_DEVICES = new MobileBrowserKnownDevices();

	private static final String REGEX_TABLETS;
	private static final String REGEX_PATTERN_TABLETS;
	private static final String REGEX_PATTERN_CONTAINS;
	private static final String REGEX_PATTERN_STARTS_WITH;

	static {
		String anchor = StringPool.OPEN_CURLY_BRACE + PROP_PREFIX + PROP_TABLETS + StringPool.CLOSE_CURLY_BRACE;

		REGEX_TABLETS = PortletProps.get(PROP_PREFIX.concat(PROP_TABLETS));
		REGEX_PATTERN_TABLETS = PortletProps.get(PROP_PREFIX.concat(PROP_PATTERN_TABLETS)).replace(anchor,REGEX_TABLETS);
		REGEX_PATTERN_CONTAINS = PortletProps.get(PROP_PREFIX.concat(PROP_PATTERN_CONTAINS)).replace(anchor,StringPool.PIPE + REGEX_TABLETS);
		REGEX_PATTERN_STARTS_WITH = PortletProps.get(PROP_PREFIX.concat(PROP_PATTERN_STARTS_WITH));

		if (_log.isDebugEnabled()) {
			_log.debug(PROP_PREFIX.concat(PROP_TABLETS) + "=" + REGEX_TABLETS);
			_log.debug(PROP_PREFIX.concat(PROP_PATTERN_TABLETS) + "=" + REGEX_PATTERN_TABLETS);
			_log.debug(PROP_PREFIX.concat(PROP_PATTERN_CONTAINS) + "=" + REGEX_PATTERN_CONTAINS);
			_log.debug(PROP_PREFIX.concat(PROP_PATTERN_STARTS_WITH) + "=" + REGEX_PATTERN_STARTS_WITH);
		}
	}
}
