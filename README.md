# Mobile Browser Device Recognition Provider #

Liferay plugin that uses regular expression rules over UA strings to detect devices. Generally the only info you need to know if is mobile browser and if is a tablet, that is what this plugin tries to detect.

The drawback of this approach is that is not possible to retrieve detailed features of the devices, but is an alternative to a repository-type plugin (such as wurfl).

Icons made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/).